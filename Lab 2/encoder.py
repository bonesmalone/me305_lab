'''!@file       encoder.py
    @brief      A driver for reading from Quadrature Encoders.
    @details    This class defines an encoder object and associated functions. 
    @author     Jarod Lyles
    @author     Simon Way
    @date       February 2, 2022
'''

import pyb
from shares import Share

class Encoder:
    '''!@brief              Interface with quadrature encoders
        @details            This class interfaces with quadrature encoders and
                            
    '''

    def __init__(self, pin_A, pin_B, timNum):
        '''!@brief      Constructs an encoder object
            @details    Initializes the encoder object by assigning object
                        parameters.
        '''
        ## Assigns the sampling time of the channels
        # 16-bit number for 65535 representing 
        self.period = 0xFFFF
        ## Initializes a timer object at given sampling period
        self.tim = pyb.Timer(timNum, period = self.period, prescaler = 0)
        ## Initializes channel 1 timer in encoder mode
        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pin_A)
        ## Initializes channel 2 timer in encoder mode
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pin_B)
        
        ## Initializes the reference count of encoder position
        self.ref_count = 0
        ## Initializes the current position count of the encoder
        self.current_pos = 0
        ## Initializes the detla count of encoder position change
        self.delta = 0
        
    def update(self):
        '''!@brief      Updates encoder position and delta
            @details
        '''
        
        ## Assigns a counter variable for updating the count
        self.update_count = self.tim.counter()
        self.delta = self.update_count - self.ref_count
        
        if self.delta > self.period/2:
            self.delta -= self.period
        elif self.delta < (-1*self.period/2):
            self.delta += self.period
            
        self.ref_count = self.update_count
        self.current_pos += self.delta
        

    def get_position(self):
        '''!@brief      Returns encoder position
            @details    
            @return     The position of the encoder shaft
        '''
        return self.current_pos

    def zero(self, zero_position):
        '''!@brief      Resets the encoder position to zero
            @details
            @param zero_position    Provides a vlaue to zero from.
        '''
        
        #self.ref_count = 0
        #self.update_count = 0
        self.current_pos = zero_position
        #self.delta = 0

    def get_delta(self):
        '''!@brief      Returns encoder delta
            @details
            @return     The change in position of the encoder shaft
                        between the two most recent updates
        '''
        return self.delta
            