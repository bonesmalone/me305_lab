'''!@file       task_user.py
    @brief      A module task for managing state transitions and user interface.
    @details    This file contains a  in the form of a task to interpret user 
                keyboard inputs and process conditionals to perform the actions 
                of the state transitions.
                The state transition diagram for this task is included below:
    @image      html Task_User.png
    @author     Jarod Lyles
    @author     Simon Way
    @date       February 2, 2022
'''


import pyb, time, array
from shares import Share

## Assigns constant value for State 0
S0_INIT = 0
## Assigns constant value for State 1
S1_WAIT_FOR_CMD = 1
## Assigns constant value for State 2
S2_ZERO_ENC = 2
## Assigns constant value for State 3
S3_PRINT_POSITION = 3
## Assigns constant value for State 4
S4_PRINT_DELTA = 4
## Assigns constant value for State 5
S5_COLLECT_DATA = 5
        
def taskUser(taskName, period, position, delta, zFlag, pFlag, dFlag, gFlag, posData, timeData):

    '''!@brief           Manages the user interfacing, state transitions, and
                         returning data to the user.
        @details         This task, taskUser(), accepts shared variables
                         for boolean flags and positional details to perform 
                         appropreiate state transitions and return values to
                         to the ueser interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param position  Initializes a passed shared integer variable upon which encoder
                         position data can be assigned.
        @param delta     Initializes a passed shared integer variable for assignment of
                         position change data can be assigned.
        @param zFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for zeroing the encoder.
        @param pFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position.
        @param dFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position change.
        @param gFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for recording data over time.
        @param posData   Initializes a passed shared variable for
                         recording position data.
        @param timeData  Initializes a passed shared variable for
                         recording time data.                 
                         
    '''
    
    intro()
    
    ## Initializes an active state variable for transition management
    state = S0_INIT
    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)
    
    ## Initializes array for storing collected time data
    timeArray = array.array('l', 3000 * [0])
    ## Initializes array for storing collected position data
    posArray = array.array('l', 3000 * [0])
    
    ## Initializes a serial port reader for user input
    serport = pyb.USB_VCP()
    
    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        if time.ticks_diff(current_time,next_time) >= 0:
            next_time = time.ticks_add(next_time, period)
            
            if state == S0_INIT:
                state = S1_WAIT_FOR_CMD
                    
            elif state == S1_WAIT_FOR_CMD:
                if serport.any():
                    
                    ## Assigns a character variable for the read out from the serial coms
                    charIn = serport.read(1).decode()
                    
                    # The following if/else tree performs the conditional logic
                    # on the user input to determine state transtion from State 1 
                    # and properly flag for use in the task encoder.
                    if charIn in {'z', 'Z'}:
                        state = S2_ZERO_ENC
                        zFlag.write(True)
                        print('Input Z - Zeroing Encoder')
                        print('Encoder position zeroed!')
                        
                    elif charIn in {'p','P'}:
                        state = S3_PRINT_POSITION
                        pFlag.write(True)
                        print('Input P – Returning Current Encoder Position')
                        print('Position')
                        
                    elif charIn in {'d','D'}:
                        state = S4_PRINT_DELTA
                        dFlag.write(True)
                        print('Input D – Returning Encoder Position Change')
                        print('Delta')
                        
                    elif charIn in {'g','G'}:
                        state = S5_COLLECT_DATA
                        print('Input G - Collect Data')
                        collection_start = time.ticks_ms()
                        gFlag.write(True)
                        index = 0
                        
                        print('                 Begin Data Collection                 ')
                        print('_______________________________________________________')
                       
                    elif charIn in {'h','H'}:
                        intro()
                        
                    else:
                        print(f'You entered {charIn}. This is not a valid command.')
            
            # The following if/else tree determines appropreaite actions per 
            # state assignment
            elif state == S2_ZERO_ENC:
                if not zFlag.read():
                    state = S1_WAIT_FOR_CMD
                
            elif state == S3_PRINT_POSITION:
                if not pFlag.read():
                    print(position.read())
                    state = S1_WAIT_FOR_CMD
                
            elif state == S4_PRINT_DELTA:
                if not dFlag.read():
                    print(delta.read())
                    state = S1_WAIT_FOR_CMD
                
            elif state == S5_COLLECT_DATA:
                # Reads the serial port for potential 'S' input
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'s', 'S'}:
                        gFlag.write(False)
                        # For loop to print out time/position data if 'S' is entered
                        for (t, p) in zip(timeArray[:index], posArray[:index]):
                            print(f't = {(t - collection_start)/1000:.2f}      ticks = {p}')
                            #print(f'{(t - collection_start)/1000:.2f}, {p}')

                        print('_______________________________________________________')
                        print('                  End Data Collection                  ')
                        state = S1_WAIT_FOR_CMD
                    
                # Conditional for 3000 data entries (30 seconds given 1000us period)
                elif index >= 3000:
                    # For loop to print ouit time/position data after 3000 entries (30 seconds)
                    gFlag.write(False)
                    for (t, p) in zip(timeArray[:index], posArray[:index]):
                        print(f't = {(t - collection_start)/1000:.2f}      ticks = {p}')
                        #print(f'{(t - collection_start)/1000:.2f}, {p}')
                        
                    print('_______________________________________________________')
                    print('                  End Data Collection                  ')
                    state = S1_WAIT_FOR_CMD
                
                # Conditional to continue adding to the time/position array otherwise
                else:
                    posArray[index] = posData.read()
                    timeArray[index] = timeData.read()
                    index += 1
                
            else:
                raise ValueError(f'Invalid state value in {taskName}: State {state} does not exist')
                
            yield state
            
        else:
            yield None
    

# Function for printing the welcome / command window
def intro():
    print(' _____________________________________________________ ')
    print('|                                                     |')
    print('|            MOTOR ENCODER TESTING PLATFORM           |')
    print('|_____________________________________________________|')
    print('| Available user commands:                            |')
    print('|  [Z] or [z]   |   Zero the encoder                  |')
    print('|  [P] or [p]   |   Print the encoder position        |')
    print('|  [D] or [d]   |   Print the change in position      |')
    print('|  [G] or [g]   |   Collect 30 seconds for data       |')
    print('|  [S] or [s]   |   End data collection prematurely   |')
    print('|  [H] or [h]   |   Provide help menu                 |')
    print('|  [CTRL]+[C]   |   Interrupt program                 |')
    print('|_____________________________________________________|\n')
    
    


