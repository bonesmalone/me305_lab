# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 08:43:01 2022

@author: Simon Way
"""

def fib (idx):
    calc = 0
    if idx == 0:    #Sets initial value of fib sequence to 0
        calc = 0
        
    #Sets second value of fib sequence to 1
    elif idx == 1:  
        calc = 1
        
    #Starts calcualtion sequence for larger index values    
    elif idx > 1:   
        idxcnt = 1       #Variable to equate positiion to user selected index
        fiblist = [0, 1] #List of fib values to recal from during calculation
        
        #Loops calcualtions so long at the index value has yet to be reached 
        while idxcnt<idx:
            #Adds current list value to previous list value to find next value
            A = fiblist[idxcnt] + fiblist[idxcnt - 1]  
            fiblist.append(A)     #Adds new fib sequence value to the list
            
            #Incriments the index counting value towards the user index value
            idxcnt +=1
        #Sets the called variable to the matching index position from the list
        calc = fiblist[-1]      
    return calc

if __name__ == '__main__':
    idx = input('Please enter index')
    try:
        idx = int(idx)    #Creates integer from index input to use of </>
        if idx < 0:       #prevents negative input values for index
            error = ValueError('Value must be posititve')
            
            #Ends program without printing the index position and value
            raise error   
    except:                      #Prevents user input of non-integer values
        print('Invalid Input')
        
    else:
        print ('Fibonacci number at '
               'index {:} is {:}. ' .format(idx,fib(idx)))