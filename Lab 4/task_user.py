'''!@file       task_user.py
    @brief      A module task for managing state transitions and user interface.
    @details    This file contains a  in the form of a task to interpret user 
                keyboard inputs and process conditionals to perform the actions 
                of the state transitions.
    @author     Jarod Lyles
    @date       February 17, 2022
'''


import pyb, time, array, micropython
from shares import Share
import gc

## Assigns constant value for State 0
S0_INIT = micropython.const(0)
## Assigns constant value for State 1
S1_WAIT_FOR_CMD = micropython.const(1)
## Assigns constant value for State 2
S2_ZERO_ENC = micropython.const(2)
## Assigns constant value for State 3
S3_PRINT_POSITION = micropython.const(3)
## Assigns constant value for State 4
S4_PRINT_DELTA = micropython.const(4)
## Assigns constant value for State 5
S5_COLLECT_DATA = micropython.const(5)
## Assigns constant value for State 6
S6_SET_DUTY_CYCLE_M1 = micropython.const(6)
## Assigns constant value for State 7
S7_SET_DUTY_CYCLE_M2 = micropython.const(7)
## Assigns constant value for State 8
S8_CLEAR_FAULT = micropython.const(8)
## Assigns constant value for State 9
S9_GET_VELOCITY = micropython.const(9)
## Assigns constant value for State 10
S10_TESTING_INTERFACE = micropython.const(10)
## Assigns constant value for State 11
S11_RECORD_TEST = micropython.const(11)
## Assigns constant value for State 12
S12_PRINT_TEST = micropython.const(12)
## Assigns constant value for State 13
S13_SET_SETPOINT = micropython.const(13)
## Assigns constant value for State 14
S14_SET_GAIN = micropython.const(14)
## Assigns constant value for State 15
S15_TOGGLE_CONTROL = micropython.const(15)
## Assigns constant value for State 16
S16_PERFORM_STEP_RESPONSE = micropython.const(16)

## Initializes a serial port reader for user input
serport = pyb.USB_VCP()
        
def taskUser(taskName, period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, mFlag, cFlag, vFlag, tFlag, yFlag, kFlag, wFlag, rFlag, velData, posData, timeData, duty1, duty2, gain, setPoint):

    '''!@brief           Manages the user interfacing, state transitions, and
                         returning data to the user.
        @details         This task, taskUser(), accepts shared variables
                         for boolean flags and positional details to perform 
                         appropreiate state transitions and return values to
                         to the ueser interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param position  Initializes a passed shared integer variable upon which encoder
                         position data can be assigned.
        @param delta     Initializes a passed shared integer variable for assignment of
                         position change data can be assigned.
        @param zFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for zeroing the encoder.
        @param pFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position.
        @param dFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position change.
        @param gFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for recording data over time.
        @param posData   Initializes a passed shared variable for
                         recording position data.
        @param timeData  Initializes a passed shared variable for
                         recording time data.                 
                         
    '''
    
    intro()
    
    ## Initializes an active state variable for transition management
    state = S0_INIT
    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)
    
    ## Initializes array for storing collected time data
    timeArray = array.array('f', 1000 * [0])
    gc.collect()
    
    ## Initializes array for storing collected position data
    posArray = array.array('f', timeArray)
    ## Initializes array for storing collected velocity data
    velArray = array.array('f', timeArray)
    
    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        if time.ticks_diff(current_time,next_time) >= 0:
            next_time = time.ticks_add(next_time, period)
            
            if state == S0_INIT:
                state = S1_WAIT_FOR_CMD
                    
            elif state == S1_WAIT_FOR_CMD:
                if serport.any():
                    
                    ## Assigns a character variable for the read out from the serial coms
                    charIn = serport.read(1).decode()
                    
                    # The following if/else tree performs the conditional logic
                    # on the user input to determine state transtion from State 1 
                    # and properly flag for use in the task encoder.
                    if charIn in {'z', 'Z'}:
                        state = S2_ZERO_ENC
                        zFlag.write(True)
                        print('Input Z - Zeroing Encoder')
                        print('Encoder position zeroed!')
                        
                    elif charIn in {'p','P'}:
                        state = S3_PRINT_POSITION
                        pFlag.write(True)
                        print('Input P – Returning Current Encoder Position')
                        print('Position')
                        
                    elif charIn in {'d','D'}:
                        state = S4_PRINT_DELTA
                        dFlag.write(True)
                        print('Input D – Returning Encoder Position Change')
                        print('Delta')
                        
                    elif charIn in {'g','G'}:
                        state = S5_COLLECT_DATA
                        print('Input G - Collect Data')
                        collection_start = time.ticks_ms()
                        gFlag.write(True)
                        index = 0
                        
                        print('            Begin Data Collection             ')
                        print('______________________________________________')
                        print('Time [s], Position [rad], Velocity [rad/s]    ')
                       
                    elif charIn in {'h','H'}:
                        intro()
                        
                    elif charIn in {'m'}:
                        if not wFlag.read():
                            print('Input m - Enter Duty Cycle [%] for Motor 1')
                            mFlag.write(True)
                            buff = ''
                            state = S6_SET_DUTY_CYCLE_M1
                        else:
                            print('Please toggle to open-loop control to set duty cycle manually')

                    elif charIn in {'M'}:
                        print('Input M - Enter Duty Cycle [%] for Motor 2')
                        if not wFlag.read():
                            mFlag.write(True)
                            buff = ''
                            state = S7_SET_DUTY_CYCLE_M2
                        else:
                            print('Please toggle to open-loop control to set duty cycle manually')
                            state = S1_WAIT_FOR_CMD
                        
                    elif charIn in {'c', 'C'}:
                        state = S8_CLEAR_FAULT
                        print('Input C - Clear Motor Fault')
                        cFlag.write(True)
                    
                    elif charIn in {'v', 'V'}:
                        state = S9_GET_VELOCITY
                        print('Input V - Get Velocity of Motor/Encoder 1')
                        vFlag.write(True)
                    
                    elif charIn in {'t', 'T'}:
                        state = S10_TESTING_INTERFACE
                        print('Input T - Open Motor Testing Interface')
                        tFlag.write(True)
                        buff = ''
                        avgVelList = []
                        dutyList = []
                        velocityList = []

                    elif charIn in {'y', 'Y'}:
                        state = S13_SET_SETPOINT
                        print('Input Y - Enter Set Point [rad/s] for Closed-Loop Control')
                        yFlag.write(True)
                        enterSetPoint = False
                        buff = ''

                    elif charIn in {'k', 'K'}:
                        state = S14_SET_GAIN
                        print('Input K - Enter Gain [%/rad/s] for Closed-Loop Control')
                        kFlag.write(True)
                        enterGain = False
                        buff = ''

                    elif charIn in {'w', 'W'}:
                        state = S15_TOGGLE_CONTROL
                        print('Input W - Toggle Closed-Loop Control On/Off')

                    elif charIn in {'r', 'R'}:
                        print('Input R - Perform Step Response')
                        if wFlag.read():
                            buff = ''
                            state = S16_PERFORM_STEP_RESPONSE
                            rFlag.write(True)
                            enterGain = False
                            enterSetPoint = False
                            index = 0
                            items = 300

                        else:
                            print("Please toggle to closed-loop control to perform a step response")
                            state = S1_WAIT_FOR_CMD
                        

                    else:
                        print(f'You entered {charIn}. This is not a valid command.')
            
            # The following if/else tree determines appropreiate actions per 
            # state assignment
            elif state == S2_ZERO_ENC:
                if not zFlag.read():
                    state = S1_WAIT_FOR_CMD
                
            elif state == S3_PRINT_POSITION:
                if not pFlag.read():
                    print(position.read())
                    state = S1_WAIT_FOR_CMD
            
            elif state == S4_PRINT_DELTA:
                if not dFlag.read():
                    print(delta.read())
                    state = S1_WAIT_FOR_CMD
                
            elif state == S5_COLLECT_DATA:
                # Reads the serial port for potential 'S' input
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'s', 'S'}:
                        gFlag.write(False)
                        # For loop to print out time/position data if 'S' is entered
                        for (t, p, v) in zip(timeArray[:index], posArray[:index], velArray[:index]):
                            print(f'{(t - collection_start)/1000:.2f},  {p},    {v}')
                            #print(f'{(t - collection_start)/1000:.2f}, {p}')

                        print('______________________________________________')
                        print('             End Data Collection              ')
                        state = S1_WAIT_FOR_CMD
                    
                # Conditional for 3000 data entries (30 seconds given 1000us period)
                elif index >= 1000:
                    # For loop to print out time/position data after 3000 entries (30 seconds)
                    gFlag.write(False)
                    for (t, p, v) in zip(timeArray[:index], posArray[:index], velArray[:index]):
                        print(f'{(t - collection_start)/1000:.2f},  {p},    {v}')
                        #print(f'{(t - collection_start)/1000:.2f}, {p}')
                        
                    print('______________________________________________')
                    print('             End Data Collection              ')
                    state = S1_WAIT_FOR_CMD
                
                # Conditional to continue adding to the time/position array otherwise
                else:
                    timeArray[index] = timeData.read()
                    posArray[index] = (posData.read() * 2 * 3.141 / 4000)
                    velArray[index] = (velData.read() * 2 * 3.141 / 4000)
                    index += 1
            
            elif state == S6_SET_DUTY_CYCLE_M1:
                if serport.any():
                    charIn = serport.read(1).decode()
        
                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        else:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        if len(buff) == 0:
                            buff = '0'
                        duty = float(buff)
                
                        if duty > 100:
                            duty = 100
                            print('\nDuty cycle can not exceed 100%')
                        elif duty < -100:
                            duty = -100
                            print('\nDuty cycle can not exceed -100%')
                        elif duty == 0:
                            duty = 0
                        duty1.write(duty)
                        print(f'\nMotor 1 duty cycle set to {duty1.read()}%')
                        state = S1_WAIT_FOR_CMD
            
            elif state == S7_SET_DUTY_CYCLE_M2:
                if serport.any():
                    charIn = serport.read(1).decode()
        
                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        else:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        if len(buff) == 0:
                            buff = '0'
                        duty = float(buff)
                        
                        if duty > 100:
                            duty = 100
                            print('\nDuty cycle can not exceed 100%')
                        elif duty < -100:
                            duty = -100
                            print('\nDuty cycle can not exceed -100%')
                        elif duty == 0:
                            duty = 0
                        duty2.write(duty)
                        print(f'\nMotor 2 duty cycle set to {duty2.read()}%')
                        state = S1_WAIT_FOR_CMD
                
            elif state == S9_GET_VELOCITY:
                if not vFlag.read():
                    print(f'{velocity.read() * 2 * 3.14 / 4000} [rad/s]')
                    state = S1_WAIT_FOR_CMD
            
            elif state == S8_CLEAR_FAULT:
                if not cFlag.read():
                    print('MOTOR FAULT CLEARED')
                    state = S1_WAIT_FOR_CMD
            
            elif state == S10_TESTING_INTERFACE:
                if serport.any():
                    charIn = serport.read(1).decode()

                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        elif len(buff) == 0:
                            pass
                        else:
                            state = S10_TESTING_INTERFACE
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        
                        if len(buff) == 0:
                            buff = '0'
                                    
                        duty = float(buff)
                        dutyList.append(duty)
                        if duty > 100:
                            duty = 100
                            print('\nDuty cycle can not exceed 100%')
                        elif duty < -100:
                            duty = -100
                            print('\nDuty cycle can not exceed -100%')
                        elif duty == 0:
                            duty = 0
                        duty1.write(duty)
                        print(f'\nMotor 1 duty cycle set to {duty1.read()}%')
                        buff = ''
                        velocityList = []
                        state = S11_RECORD_TEST
                        print('Enter a duty cycle for Motor 1 or enter [S] to exit')
                    
                    elif charIn in {'s','S'}:
                        state = S12_PRINT_TEST
                        
                    else:
                        state = S10_TESTING_INTERFACE
                           
                
            elif state == S11_RECORD_TEST:
                if len(velocityList) >= 10:
                    avgVelList.append(sum(velocityList)/len(velocityList))
                    state = S10_TESTING_INTERFACE
                else:
                    vFlag.write(True)
                    velocityList.append(velocity.read()/1_000)
                #print(velocityList)

            elif state == S12_PRINT_TEST:
                if len(dutyList) > 0:
                    print('Duty Cycle [%], Speed [rad/s]')
                    for (pos, vel) in zip(dutyList, avgVelList):
                        print(f'{pos}, {vel}')
                    print('Exiting Testing Interface')
                    state = S1_WAIT_FOR_CMD
                    pass
                else:
                    state = S1_WAIT_FOR_CMD

            elif state == S13_SET_SETPOINT:
                if serport.any():
                    charIn = serport.read(1).decode()

                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        elif len(buff) == 0:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        
                        if len(buff) == 0:
                            buff = '0'
                        
                        if enterSetPoint:
                            vel_ref = float(buff) / (2 * 3.141 / 4000)
                            setPoint.write(0)
                            collection_start = time.ticks_ms()
                            state = S16_PERFORM_STEP_RESPONSE
                        else:
                            setPoint.write(float(buff) / (2 * 3.141 / 4000))
                            print(f'\nSet point set to {buff} rad/s')
                            state = S1_WAIT_FOR_CMD


            elif state == S14_SET_GAIN:
                if serport.any():
                    charIn = serport.read(1).decode()

                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        elif len(buff) == 0:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        
                        if len(buff) == 0:
                            buff = '0'

                        if enterGain:
                            #K_p = float(buff) * (2 * 3.141 / 4000)
                            gain.write(float(buff) * (2 * 3.141 / 4000))
                            print(f'\nGain set to {buff} %/(rad/s)')
                            state = S16_PERFORM_STEP_RESPONSE
                        else:
                            gain.write(float(buff) * (2 * 3.141 / 4000))
                            print(f'\nGain set to {buff} %/(rad/s)')
                            state = S1_WAIT_FOR_CMD

 

            elif state == S15_TOGGLE_CONTROL:
                if wFlag.read():
                    print("Closed-Loop Control DISABLED")
                    wFlag.write(False)
                    yield wFlag
                    state = S1_WAIT_FOR_CMD
                elif not wFlag.read():
                    print("Closed-Loop Control ENABLED")
                    wFlag.write(True)
                    yield wFlag
                    state = S1_WAIT_FOR_CMD
                

            elif state == S16_PERFORM_STEP_RESPONSE:
                gFlag.write(True)
                if not enterGain and not enterSetPoint:
                    print("Enter a Proportional Gain [%/(rad/s)]:")
                    enterGain = True
                    buff = ''
                    state = S14_SET_GAIN
                elif enterGain and not enterSetPoint:
                    print("Enter a Set Point [rad/s]:")
                    enterSetPoint = True
                    buff = ''
                    state = S13_SET_SETPOINT
                elif enterGain and enterSetPoint:
                    if index <= 100:
                        timeArray[index] = timeData.read()
                        velArray[index] = (velData.read() * 2 * 3.141 / 4000)
                        posArray[index] = (duty1.read())
                        index += 1
                    elif index > 100 and index < items:
                        #gain.write(K_p)
                        setPoint.write(vel_ref)
                        timeArray[index] = timeData.read()
                        velArray[index] = (velData.read() * 2 * 3.141 / 4000)
                        posArray[index] = (duty1.read())
                        index += 1
                    else:
                        
                        print('            Start Data Collection             ')
                        print('time [s], velocity [rad/s], actuation [%]     ')
                        print('______________________________________________')
                        for (t, v, L) in zip(timeArray[:index], velArray[:index], posArray[:index]):
                            print(f'{(t - collection_start)/1000:.2f},  {v},    {L}')
                        print('______________________________________________')
                        print('             End Data Collection              ')
                        gFlag.write(False)
                        state = S1_WAIT_FOR_CMD
                
                    
            else:
                raise ValueError(f'Invalid state value in {taskName}: State {state} does not exist')
                
            yield state
            
        else:
            yield None
    

# Function for printing the welcome / command window
def intro():
    print(' _____________________________________________________ ')
    print('|                                                     |')
    print('|            MOTOR ENCODER TESTING PLATFORM           |')
    print('|_____________________________________________________|')
    print('| Available user commands:                            |')
    print('|  [Z] or [z]   |   Zero the Encoder 1                |')
    print('|  [P] or [p]   |   Print Encoder 1 position          |')
    print('|  [D] or [d]   |   Print the change in position      |')
    print('|  [V] or [v]   |   Print Velocity for Encoder 1      |')
    print('|         [m]   |   Enter Motor 1 duty cycle          |')
    print('|  [M]          |   Enter Motor 2 duty cycle          |')
    print('|  [C] or [c]   |   Clear motor fault condition       |')
    print('|  [G] or [g]   |   Collect 30 seconds of data        |')
    print('|  [T] or [t]   |   Open motor testing interface      |')
    print('|  [S] or [s]   |   End data collection prematurely   |')
    print('|  [H] or [h]   |   Provide help menu                 |')
    print('|  [W] or [w]   |   Toggle on/off closed-loop control |')
    print('|  [K] or [k]   |   Enter a proportional gain value   |')
    print('|  [Y] or [y]   |   Enter a set point value           |')
    print('|  [R] or [r]   |   Perform step input response       |')
    print('|  [CTRL]+[C]   |   Interrupt program                 |')
    print('|_____________________________________________________|\n')
