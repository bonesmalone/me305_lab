'''!@file       task_motor.py
@brief      A module for tasking the motor to enable the driver DRV8847.
@details    This module initializes motor objects and tasks the motor to the appropreiate
            frequency of operation.          
@author     Jarod Lyles
@date       February 2, 2022
'''

import pyb, time
import DRV8847
from shares import Share
import closedLoop


## Initializes pin B4 on the Nucleo for interface with the first motor
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
## Initializes pin B5 on the Nucleo for interface with the first motor
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
## Initializes pin B0 on the Nucleo for interface with the second motor
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## Initializes pin B6 on the Nucleo for interface with the second motor
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)


def taskMotor(taskName, period, cFlag, wFlag, duty1, duty2, gain, setPoint, velocity):
    '''!@brief           Manages the initialization and tasking of the motor drivers.
        @details         This task, taskMotor(), accepts shared variables
                         for boolean flags and duty cycle details to perform 
                         PWM drive on the motors.
        @param taskName  Assigns a name to the instance of the taskMotor task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param cFlag     Initializes a passed shared boolean flag for
                         clearing the motor from user input.
        @param duty1     Assigns a value for a shared object containing the requested
                         PWM signal for motor 1.
        @param duty2     Assigns a value for a shared object containing the requested
                         PWM signal for motor 2.
    '''
    
    ## Initializes the motor DRV8847 object
    drv = DRV8847.DRV8847()

    closedLoopController = closedLoop.ClosedLoop()

    ## Initializes Motor 1 object using the DRV8847 class object
    motor_1 = drv.motor(20_000, 1, pinB4, 2, pinB5)
    ## Initializes Motor 1 object using the DRV8847 class object
    motor_2 = drv.motor(20_000, 3, pinB0, 4, pinB1)

    ## Enable the DRV8847 driver
    drv.enable()

    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes first iteration time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)

    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        
        ## 
        if time.ticks_diff(current_time, next_time) >= 0:
            # Set the duty cycle of the motors using shared variables
            
            motor_1.set_duty(duty1.read())
            motor_2.set_duty(duty2.read())

            if wFlag.read():
                closedLoopController.set_gain(gain.read())
                closedLoopController.update(setPoint.read(), velocity.read())
                motor_1.set_duty(closedLoopController.L)
                duty1.write(closedLoopController.L)
            else:
                pass
                    
            # In the case of a motor fault
            if cFlag.read():
                # Re-enable the driver
                drv.enable()
                cFlag.write(False)
            
            # Provides update time for the loop
            next_time = time.ticks_add(next_time, period)
                
        else:
            yield None
