'''!@file               task_encoder.py
    @brief              A module for tasking the closed loop controller system
    @details            The module initializes closed loop objects and tasks it to correct the actuation values
    @author             Jarod Lyles
    @author             Simon Way
    @date               February 2, 2022
'''


import pyb, time, encoder
from shares import Share

pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
             

def taskEncoder(taskName, period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, vFlag, velData, posData, timeData):
    '''!@brief              Manages initializtion and tasking of closed loop driver
        @details            This task acepts shared variables from boolean flags and positional data
                            to perform closed loop control corrections.
        @param taskName     Assigns a name to the instance of the taskencoder task.
        @param zFlag        initializes a passed shared boolean flag for zeroing the 
                            encoders position setting
        @param pFlag        Initializes a passed shared boolean flag for prompting the 
                            encoder task for pritnting the encoders current position.
        @param dFlag        Initializes a passed shared boolean flag for prompting the 
                            encoder task to print the delta value
    '''
    
    encoder1 = encoder.Encoder(pinB6, pinB7, 4)
    
    start_time = time.ticks_us()
    next_time = time.ticks_add(start_time, period)
    prev_time = time.ticks_us()
    
    while True:
        current_time = time.ticks_us()
        
        # Updates the encoder
        encoder1.update()
        
        # Performs conditional assignment of encoder functions
        if current_time >= next_time:
            next_time = time.ticks_add(start_time, period)
            
            # Perform instantaneous velocity calculation and write to shared variable
            delta_time = time.ticks_diff(current_time, prev_time) / 1_000_000
            #vel = encoder1.get_position() / delta_time
            vel = encoder1.get_delta() / delta_time
            velocity.write(vel)

            # Tasks the encoder for zeroing of position
            if zFlag.read():
                encoder1.zero(0)
                zFlag.write(False)
            
            # Tasks the encoder to get position
            elif pFlag.read():
                position.write(encoder1.get_position())
                pFlag.write(False)
            
            # Tasks the encoder to get the instantaneous postional delta
            elif dFlag.read():
                delta.write(encoder1.get_delta())
                dFlag.write(False)
            
            # Tasks the encoder to collect data – update and store values in arrays 
            elif gFlag.read():
                encoder1.update()
                timeData.write(time.ticks_ms())
                ##posData.write(encoder1.get_delta())
                posData.write(encoder1.get_position())
                velData.write(vel)
            
            # Tasks the encoder to provide velocity value
            elif vFlag.read():
                # Velocity = delta / time period of update
                delta_time = time.ticks_diff(current_time, prev_time) / 1_000_000
                vel = encoder1.get_delta() / delta_time
                #vel = encoder1.get_position() / delta_time
                velocity.write(vel)
                vFlag.write(False)
            
            # Saves the current state of the function and returns values
            yield None
            prev_time = current_time    
        else:
            yield None
                    
            
        