'''!@file       main.py
    @brief      A testing program for reading and recording Quadrature encoder 
                position data.
    @details    The main file executes the necessary tasks to carry out the program
                assigns necessary shared variables amongst source code files.
    @author     Jarod Lyles
    @date       February 3, 2022
'''

import shares
import task_user, task_encoder, task_motor

## Assigns a shared boolean flag to manage zeroing the encoder
zFlag = shares.Share(False)
## Assigns a shared boolean flag to manage getting encoder position
pFlag = shares.Share(False)
## Assigns a shared boolean flag to manage getting the encoder position delta
dFlag = shares.Share(False)
## Assigns a shared boolean flag to manage collecting encoder data
gFlag = shares.Share(False)
## Assigns a shared boolean flag to manage setting duty cycle
mFlag = shares.Share(False)
## Assigns a shared boolean flag to manage setting motor fault conditions
cFlag = shares.Share(False)
## Assigns a shared boolean flag to manage 
vFlag = shares.Share(False)
## Assigns a shared boolean flag to manage testing interface
tFlag = shares.Share(False)

yFlag = shares.Share(False)

wFlag = shares.Share(False)

kFlag = shares.Share(False)

rFlag = shares.Share(False)


## Assigns a shared position variable for handling of encoder position
position = shares.Share(0)
## Assigns a shared position variable for handling of encoder position delta
delta = shares.Share(0)
## Assigns a shared velocity varibale for handling of encoder read velocity
velocity = shares.Share(0)

duty = shares.Share(0)

## Assigns a shared variable for collection of encoder position data
posData = shares.Share(0)
## Assigns a shared variable for collection of encoder time data
timeData = shares.Share(0)
## Assigns a shared variable for collection of encoder velocity data
velData = shares.Share(0)

## Assigns a shared variable for motor 1 duty cycle
duty1 = shares.Share(0)
## Assigns a shared variable for motor 2 duty cycle
duty2 = shares.Share(0)

gain = shares.Share(0)

setPoint = shares.Share(0)

## Defines the period of operation of the task functions in microseconds
period = 10_000
    
if __name__ == '__main__':
    
    ## Initializes the user task
    task1 = task_user.taskUser('taskUser', period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, mFlag, cFlag, vFlag, tFlag, yFlag, kFlag, wFlag, rFlag, velData, posData, timeData, duty1, duty2, gain, setPoint)
    ## Intializes the encoder task
    task2 = task_encoder.taskEncoder('taskEncoder', period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, vFlag, velData, posData, timeData)
    ## Initializes the motor task
    task3 = task_motor.taskMotor('taskMotor', period, cFlag, wFlag, duty1, duty2, gain, setPoint, velocity)
    
    taskList = [task1, task2, task3]
    
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
        
        
print('PROGRAM TERMINATING')
    
