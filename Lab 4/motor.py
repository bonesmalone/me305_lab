'''!@file       motor.py
    @brief      A driver for controlling the board motors.
    @details    This class defines a motor object and the control of the assigned duty cycle.
    @author     Jarod Lyles
    @date       Febuary 2, 2022
'''


from pyb import Pin, Timer

class Motor: 
    '''!@brief          Interface with motors
        @details        This class interfaces with the motors and allows for setting their duty cycles.
        
    '''
     
    def __init__(self, freq, outA, IN_A_pin, outB, IN_B_pin): 
        '''!@brief      Contructs a motor object
            @details    Initializes the motor object by assigning object parameters.
        '''
         
        self.PWM_tim = Timer(3, freq = freq) 
        
        # Pin initialization 
        self.IN_A = Pin(IN_A_pin, mode=Pin.OUT_PP) 
        self.IN_B = Pin(IN_B_pin, mode=Pin.OUT_PP) 
        
        self.t3chA = self.PWM_tim.channel(outA, Timer.PWM_INVERTED, pin=self.IN_A) 
        self.t3chB = self.PWM_tim.channel(outB, Timer.PWM_INVERTED, pin=self.IN_B) 
         
        pass 
     
    def set_duty(self, duty): 
        '''!@brief      Sets motor duty cycle
            @details    This function sets the duty cycle of the motors from the input.
        '''
        if duty > 0: 
            self.t3chA.pulse_width_percent(abs(duty))
            self.t3chB.pulse_width_percent(0)
        elif duty < 0: 
            self.t3chA.pulse_width_percent(0)
            self.t3chB.pulse_width_percent(abs(duty))
        pass

