'''!@file       task_ClosedLoop.py
    @brief      A module for tasking the closed loop driver to perform calculations.
    @details    This file contains a module in the form of a task that initializes the
                closedloop driver and performs the appropriate calculations.
    @author     Simon Way
    @date       Febuary 22, 2022
'''

import pyb, time, ClosedLoop
from shares import Share

pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)

def taskClosedLoop(taskname, period, kFlag, kp1):
    
    '''!@brief              Manages closed loop control and calibration
        @details            This task, task_ClosedLoop(), uses shared variables and boolean
                            flags for actuation values and calibration details to 
                            perform appropriate state transitions and return adjusted
                            values of duty cycle to the circuit when active. 
        @param taskname     Assigns name to the taskClosedLoop task.
        @param period       Provides the period of update in microseconds
        @param kFlag        Initializes a passed shared boolean flag for evaluation
                            and assignment of user input details, specifically for
                            seting the gain value.
                            
    '''
    
    ClosedLoop1 = ClosedLoop.Closed_Loop(pinB6, pinB7, 4)
    
    while True:
        if kFlag.read():
            ##print('Hey you get how flags work. Good job dude!')
            ClosedLoop1.set_kp(kp1.read())
            print(f'\nKp value set to {kp1.read()}')
            kFlag.write(False)
        else:
            yield None