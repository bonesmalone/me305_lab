'''!@file           ClosedLoop.py
    @brief          A driver for controlling the closed loop system
    @details        This class defines the closed loop controller object and control of actuation cycle to the motor
    @author         Simon Way
    @author         Jarod Lyles
    @date           Febuary 3, 2022
'''

class ClosedLoop:
    '''!@brief      Closed Loop Control
        @details    This class operates the closed loop controls of the 
    '''

    def init(self):
        '''!@brief          Contructs Closed Loop Object
            @details        Initalizes the Closed loop ibject by assigning object paramters
        '''
        ## Initializes proportional gain constant
        self.K_p = 1
        ## Initializes max saturation limit of controller
        self.max = 100
        ## Initializes saturation limit of controller
        self.min = -100
        ## Initializes value for adjusted duty cycle
        self.L = 0

    def update(self, vel_ref, vel_meas):
        '''!@brief          Updates Object parameters
            @details        This method continually updates the parameters of the closed loop object when prompted.
        '''
        
        self.vel_ref = vel_ref
        self.vel_meas = vel_meas
        #print(self.vel_meas)
        #print(self.vel_ref * 2 * 3.14 / 4000)
        #print(self.K_p / (2 * 3.14 / 4000))
        self.L = self.K_p * (vel_ref - vel_meas)
        #print(self.L)
        '''
        if self.L > self.max:
            self.L = self.max
        elif self.L < self.min:
            self.L = self.min
        '''
        return self.L

    def set_gain(self, gain):
        '''!@brief          Sets the gains value
            @details        This method applies the user input gains value to the function parameters
        '''
        
        self.K_p = gain
    
    def get_gain(self):
        '''!@brief          Returns the current gains value
            @details        This method returns the current gains value for outsied analysis
        '''

        return self.K_p

