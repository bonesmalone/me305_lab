'''!@file           task_panel.py
    @brief          tbd
'''

from pyb import Pin, ADC
from micropython import const
import time, panel

def taskPanel(taskName, period, zFlag, posx, posy):
    pan = panel.TouchPanel()
    start = time.ticks_us()
    
    prev_y = 0
    prev_x = 0
    prev_VX = 0
    prev_VY = 0
    prev_time = time.ticks_us()
    
    while True:
        meas_strt = time.ticks_us()
        x, y, z = pan.scan()
        deltT = time.ticks_diff(meas_strt, prev_time)
        
        #print(f'{x},   {y},    {z},   {deltT}')
        
        Xout = prev_x + 0.85 * (x - prev_x) + deltT * prev_VX
        Yout = prev_y + 0.85 * (y - prev_y) + deltT * prev_VY
        
        VXout = prev_VX + (0.05 / deltT) * (x - prev_x)
        VYout = prev_VY + (0.05 / deltT) * (y - prev_y)
        
        prev_x = Xout
        prev_y = Yout
        prev_VX = VXout
        prev_VY = VYout
        
        if z == False:
            Xout = 0
            Yout = 0
            VXout = 0
            VYout = 0
            
            posx.write(Xout)
            posy.write(Yout)
            zFlag.write(False)
            
        else:
            posx.write(Xout)
            posy.write(Yout)
            zFlag.write(True)