from pyb import Pin, ADC
import time
class TouchPanel():

    def __init__(self):
        self.A0 = Pin.cpu.A0
        self.A1 = Pin.cpu.A1
        self.A6 = Pin.cpu.A6
        self.A7 = Pin.cpu.A7
        self.OUT = Pin.OUT_PP
        self.IN = Pin.IN
        (self.K_xx, self.K_xy, self.K_yx, self.K_yy, self.x_0, self.y_0) = (1, 0, 0, 1, 0, 0)

    def x_scan(self):
        self.ADC_x = ADC(self.A0)
        self.X_p = Pin(self.A7, self.OUT)
        self.X_m = Pin(self.A1, self.OUT)
        self.Y_p = Pin(self.A6, self.IN)
        self.X_p.high()
        self.X_m.low()
        self.X = self.ADC_x.read()
        return self.X
    
    def y_scan(self):
        self.ADC_y = ADC(self.A1)
        self.Y_p = Pin(self.A6, self.OUT)      
        self.Y_m = Pin(self.A0, self.OUT)
        self.X_p = Pin(self.A7, self.IN)
        self.Y_p.high()
        self.Y_m.low()
        self.Y = self.ADC_y.read()
        return self.Y
    
    def z_scan(self):
        self.ADC_z = ADC(self.A7)
        self.Y_p = Pin(self.A6, self.OUT)
        self.X_m = Pin(self.A1, self.OUT)
        self.Y_m = Pin(self.A0, self.IN)
        self.Y_p.high()
        self.X_m.low()
        
        if self.ADC_z.read() < 20:
            self.contact = False
        else:
            self.contact = True

        return self.contact

    def read_position(self):
        return(self.X, self.Y, self.contact)

    def set_calibration(self, cal_coeff):
        (self.Kxx,self.Kxy,self.Kyx,self.Kyy,self.x0,self.y0) = cal_coeff

if __name__ == "__main__":
    panel = TouchPanel()
    start = time.ticks_us()
    
    panel.x_scan()
    panel.y_scan()
    panel.z_scan()
    panel.read_position()
    end = time.ticks_us()
    print(panel.read_position())
    print(f'{time.ticks_diff(end, start)} us scan time')
