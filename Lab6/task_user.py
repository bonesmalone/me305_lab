'''!@file       task_user.py
    @brief      A module task for managing state transitions and user interface.
    @details    This file contains a  in the form of a task to interpret user 
                keyboard inputs and process conditionals to perform the actions 
                of the state transitions.
    @author     Jarod Lyles
    @author     Simon Way
    @date       February 17, 2022
'''


import pyb, time, array, micropython
from shares import Share
import gc

## Assigns constant value for State 0
S0_INIT = micropython.const(0)
## Assigns constant value for State 1
S1_WAIT_FOR_CMD = micropython.const(1)
## Assigns constant value for State 2
S2_CALIBRATE = micropython.const(2)
## Assigns constant value for State 3
S3_SET_GAINS = micropython.const(3)
## Assigns constant value for State 4
S4_GAINS_2 = micropython.const(4)
## Assigns constant value for State 5
S5_BALANCE = micropython.const(5)
## Assigns constant value for State 6
S6_BALANCE_BALL = micropython.const(6)
## Assigns constant value for State 7
S7_PRINT_POSITION = micropython.const(7)
## Assigns constant value for State 8
S8_COLLECT_DATA = micropython.const(8)
## Assigns constant value for State 9
S9_PRINT_DATA = micropython.const(9)


'''
## Assigns constant value for State 8
S8_CLEAR_FAULT = micropython.const(8)
## Assigns constant value for State 9
S9_GET_VELOCITY = micropython.const(9)
## Assigns constant value for State 10
S10_TESTING_INTERFACE = micropython.const(10)
## Assigns constant value for State 11
S11_RECORD_TEST = micropython.const(11)
## Assigns constant value for State 12
S12_PRINT_TEST = micropython.const(12)
## Assigns constant value for State 13
S13_SET_SETPOINT = micropython.const(13)
## Assigns constant value for State 14
S14_SET_GAIN = micropython.const(14)
## Assigns constant value for State 15
S15_TOGGLE_CONTROL = micropython.const(15)
## Assigns constant value for State 16
S16_PERFORM_STEP_RESPONSE = micropython.const(16)
'''
## Initializes a serial port reader for user input
serport = pyb.USB_VCP()
        
def taskUser(taskName, period, cFlag, kFlag, bFlag, BFlag, pFlag, zFlag, Kp, Kd, posx, posy):

    '''!@brief           Manages the user interfacing, state transitions, and
                         returning data to the user.
        @details         This task, taskUser(), accepts shared variables
                         for boolean flags and positional details to perform 
                         appropreiate state transitions and return values to
                         to the ueser interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param position  Initializes a passed shared integer variable upon which encoder
                         position data can be assigned.
        @param delta     Initializes a passed shared integer variable for assignment of
                         position change data can be assigned.
        @param zFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for zeroing the encoder.
        @param pFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position.
        @param dFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position change.
        @param gFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for recording data over time.
        @param posData   Initializes a passed shared variable for
                         recording position data.
        @param timeData  Initializes a passed shared variable for
                         recording time data.                 
                         
    '''
    
    intro()
    
    ## Initializes an active state variable for transition management
    state = S0_INIT
    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)
    
    ## Initializes array for storing collected time data
    timeArray = array.array('f', 1000 * [0])
    gc.collect()
    
    ## Initializes array for storing collected position data
    #posArray = array.array('f', timeArray)
    ## Initializes array for storing collected velocity data
    #velArray = array.array('f', timeArray)
    
    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        if time.ticks_diff(current_time,next_time) >= 0:
            next_time = time.ticks_add(next_time, period)
            
            if state == S0_INIT:
                state = S1_WAIT_FOR_CMD
                    
            elif state == S1_WAIT_FOR_CMD:
                if serport.any():
                    
                    ## Assigns a character variable for the read out from the serial coms
                    charIn = serport.read(1).decode()
                    
                    # The following if/else tree performs the conditional logic
                    # on the user input to determine state transtion from State 1 
                    # and properly flag for use in the task encoder.
                    if charIn in {'c', 'C'}:
                        state = S2_CALIBRATE
                        cFlag.write(True)
                        print('Input C - Beginning Calibration')
                        #print('Encoder position zeroed!')
                        
                    elif charIn in {'k','K'}:
                        state = S3_SET_GAINS
                        kFlag.write(True)
                        buff = ''
                        print('Input K – Please Enter New Gain Values')
                        print('Enter Kp Value')
                        #print('Position')
                        
                    elif charIn in {'b'}:
                        state = S5_BALANCE
                        bFlag.write(True)
                        print('Input b – Balancing Platform Without Ball')
                        #print('Delta')
                        
                    elif charIn in {'B'}:
                        state = S6_BALANCE_BALL
                        BFlag.write(True)
                        print('Input B - Balancing Platform With Ball')
                        
                       
                    elif charIn in {'p','P'}:
                        state = S7_PRINT_POSITION
                        pFlag.write(True)
                        print('Input P - Printing Current Positon')

                                                
                    else:
                        print(f'You entered {charIn}. This is not a valid command.')
                                    
                    
            elif state == S2_CALIBRATE:
                print('Calirbation complete')
                state = S1_WAIT_FOR_CMD
                pass
            
            elif state == S3_SET_GAINS:
                if serport.any():
                    charIn = serport.read(1).decode()

                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        elif len(buff) == 0:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        
                        if len(buff) == 0:
                            buff = '0'
                        Kp.write(float(buff))
                        print(f'\nKp set to {buff}')
                        print('Enter Kd Value')
                        state = S4_GAINS_2
                        
            elif state == S4_GAINS_2:
                if serport.any():
                    charIn = serport.read(1).decode()

                    if charIn.isdigit():
                        buff += charIn
                        print(charIn, end='' )
                        
                    elif charIn in {'-'}: 
                        if len(buff) == 0:
                            buff += charIn
                            print(charIn, end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}: 
                        if len(buff) > 0:
                            buff = buff[:-1]
                            print('')
                            print(buff)
                        elif len(buff) == 0:
                            pass
                            
                    elif charIn in {'.'}:
                        buff += charIn
                        print(charIn, end='')
                        
                    elif charIn in {'\n','\r'}:
                        
                        if len(buff) == 0:
                            buff = '0'
                        Kd.write(float(buff))
                        print(f'\nKd set to {buff}')
                        state = S1_WAIT_FOR_CMD



   
            
            elif state == S5_BALANCE:
                print('Platform Balancing Engaged')
                state = S1_WAIT_FOR_CMD
                pass

            elif state == S6_BALANCE_BALL:
                print('Platform Ball Balancing Engaged')
                state = S1_WAIT_FOR_CMD
                pass
            
            elif state == S7_PRINT_POSITION:
                if not zFlag.read:
                    print('No ball present on platform')
                else:
                    print('x         y')
                    print(posx,         posy)
                
                
                #print('Ball currently located at [PLACEHOLDER POSITION]')
                state = S1_WAIT_FOR_CMD
                pass
            
                    
            else:
                raise ValueError(f'Invalid state value in {taskName}: State {state} does not exist')
                
            yield state
            
        else:
            yield None
    

# Function for printing the welcome / command window
def intro():
    print(' _____________________________________________________ ')
    print('|                                                     |')
    print('|            MOTOR ENCODER TESTING PLATFORM           |')
    print('|_____________________________________________________|')
    print('| Available user commands:                            |')
    print('|  [C] or [c]   |   Begin System calibration          |')
    print('|  [K] or [k]   |   Set Gains values                  |')
    print('|         [b]   |   Balance Platform w/o ball         |')
    print('|  [B]          |   Balance system with ball          |')
    print('|  [P] or [p]   |   Print current position of ball    |')
    print('|  [D] or [d]   |   Collect and Print Data            |')
    print('|_____________________________________________________|\n')
