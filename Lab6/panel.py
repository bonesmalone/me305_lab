from pyb import Pin, ADC
from micropython import const
import time
class TouchPanel():

    def __init__(self):
        self._A0 = const(Pin.cpu.A0)
        self._A1 = const(Pin.cpu.A1)
        self._A6 = const(Pin.cpu.A6)
        self._A7 = const(Pin.cpu.A7)
        self._OUT = const(Pin.OUT_PP)
        self._IN = const(Pin.IN)
        #(self.K_xx, self.K_xy, self.K_yx, self.K_yy, self.x_0, self.y_0) = (1, 0, 0, 1, 0, 0)

    def scan(self):
        self._ADC_x = ADC(self._A0)
        self._X_p = Pin(self._A7, self._OUT)
        self._X_m = Pin(self._A1, self._OUT)
        self._Y_p = Pin(self._A6, self._IN)
        self._X_p.high()
        self._X_m.low()
        self._X = self._ADC_x.read()
        
        self._ADC_y = ADC(self._A1)
        self._Y_p = Pin(self._A6, self._OUT)      
        self._Y_m = Pin(self._A0, self._OUT)
        self._X_p = Pin(self._A7, self._IN)
        self._Y_p.high()
        self._Y_m.low()
        self._Y = self._ADC_y.read()
    
        self._ADC_z = ADC(self._A7)
        self._Y_p = Pin(self._A6, self._OUT)
        self._X_m = Pin(self._A1, self._OUT)
        self._Y_m = Pin(self._A0, self._IN)
        self._Y_p.high()
        self._X_m.low()
        
        if self._ADC_z.read() > 100:
            self._contact = True
        else:
            self._contact = False

        return (self._X, self._Y, self._contact)

    '''
    def centroid(self):
        points = 0
        data = [6 * (0, 0)]
        if points < 6:
            if self.ADC_z.read() > 100:
                (x, y, z) = TouchPanel.scan()
                data[points] = (x, y)
                points += 1
            else:
                print(data)
    '''
    def set_calibration(self, cal_coeff):
        (self.K_xx, self.K_xy, self.K_yx, self.K_yy, self.x_0, self.y_0) = (1, 0, 0, 1, 0, 0)

'''if __name__ == "__main__":
    panel = TouchPanel()
    start = time.ticks_us()
    panel.scan()
    end = time.ticks_us()
    print(panel.scan())
    print(f'{time.ticks_diff(end, start)} us scan time')
    #while True:
    #    print(panel.scan())
    
    prev_y = 0
    prev_x = 0
    prev_VX = 0
    prev_VY = 0
    prev_time = time.ticks_us()
    
    while True:
        meas_strt = time.ticks_us()
        x, y, z = panel.scan()
        deltT = time.ticks_diff(meas_strt, prev_time)
        
        #print(f'{x},   {y},    {z},   {deltT}')
        
        Xout = prev_x + 0.85 * (x - prev_x) + deltT * prev_VX
        Yout = prev_y + 0.85 * (y - prev_y) + deltT * prev_VY
        
        VXout = prev_VX + (0.05 / deltT) * (x - prev_x)
        VYout = prev_VY + (0.05 / deltT) * (y - prev_y)
        
        prev_x = Xout
        prev_y = Yout
        prev_VX = VXout
        prev_VY = VYout
        
        if z == False:
            Xout = 0
            Yout = 0
            VXout = 0
            VYout = 0
            
        else:
            pass
        
        prev_time = meas_strt
        
        print(f'{Xout},      {Yout},       {z},      {VXout},       {VYout}')
'''