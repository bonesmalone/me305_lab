'''!@file       main.py
    @brief      A testing program for reading and recording Quadrature encoder 
                position data.
    @details    The main file executes the necessary tasks to carry out the program
                assigns necessary shared variables amongst source code files.
    @author     Jarod Lyles
    @date       February 3, 2022
'''

import shares
import task_user, task_panel
#, task_motor, task_control, task_IMU, task_data

## Assigns a shared boolean flag to manage calibration
cFlag = shares.Share(False)
## Assigns a shared boolean flag to manage setting gains
kFlag = shares.Share(False)
## Assigns a shared boolean flag to manage balancing the platform
bFlag = shares.Share(False)
## Assigns a shared boolean flag to manage balancing the ball
BFlag = shares.Share(False)
## Assigns a shared boolean flag to manage printing the position
pFlag = shares.Share(False)
## Assigna a shared boolean flag to manage ball panel contact
zFlag = shares.Share(False)

## Assigns a shared position variable for handling of encoder position
position = shares.Share(0)
## Assigns a shared position variable for handling of encoder position delta
delta = shares.Share(0)
## Assigns a shared velocity varibale for handling of encoder read velocity
velocity = shares.Share(0)

duty = shares.Share(0)

## Assigns a shared variable for collection of encoder position data
posData = shares.Share(0)
## Assigns a shared variable for collection of encoder time data
timeData = shares.Share(0)
## Assigns a shared variable for collection of encoder velocity data
velData = shares.Share(0)

## Assigns a shared variable for motor 1 duty cycle
duty1 = shares.Share(0)
## Assigns a shared variable for motor 2 duty cycle
duty2 = shares.Share(0)

gain = shares.Share(0)

Kp = shares.Share(0)
Kd = shares.Share(0)

posx = shares.Share(0)
posy = shares.Share(0)

setPoint = shares.Share(0)

## Defines the period of operation of the task functions in microseconds
period = 10_000
    
if __name__ == '__main__':
    
    ## Initializes the user task
    task1 = task_user.taskUser('taskUser', period, cFlag, kFlag, bFlag, BFlag, pFlag, zFlag, Kp, Kd, posx, posy)
    ## Intializes the encoder task
    task2 = task_panel.taskPanel('taskPanel', period, zFlag, posx, posy)
    ## Initializes the motor task
    #task3 = task_motor.taskMotor('taskMotor', period, cFlag, wFlag, duty1, duty2, gain, setPoint, velocity)
    
    taskList = [task1]
    
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
        
        
print('PROGRAM TERMINATING')
    
