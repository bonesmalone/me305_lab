'''!@file       BNO055.py
    @brief      A driver class for reading from the BNO055
    @details    The class defines the BNO055 object and associated functions.
    @author     Simon Way
    @author     Jarod Lyles
    @date       Febuary 24, 2022
'''

from pyb import I2C

class BNO055:
    '''!@brief      tbd
        @details    tbd
    '''
    
    def __init__(self):
        '''!@brief      tbd
            @details    tbd
        '''
        #defs
        self.i2c = I2C(1)                             # create on bus 1
        self.i2c = I2C(1, I2C.CONTROLLER)             # create and init as a controller
        self.i2c.init(I2C.CONTROLLER) # init as a controller
        #self.i2c.init(I2C.PERIPHERAL, addr=0x00)      # init as a peripheral with given address
        #self.i2c.deinit()                             # turn off the I2C unit
        self.i2c.mem_write(12, 40, 0x3D)
        #print(i2c.scan())
    
    def readMem(self):
        buf = bytearray([0, 0, 0, 0, 0, 0])
        return self.i2c.mem_read(buf, 40, 0x1A)

        
    def Op_Mode_Change(self):
        '''!@brief      tbd
            @details    tbd
        '''
        
        
    def Calib_Stat(self):
        '''!@brief      tbd
            @details    tbd
        '''
        
    def Calib_Coef(self):
        '''!@brief      tbd
            @details    tbd
        '''
        
    def Write_Calib(self):
        '''!@brief      tbd
            @details    tbd
        '''
        
    def Euler_Ang(self):
        '''!@brief      tbd
            @details    tbd
        '''
        
    def Ang_Vel(self):
        '''!@brief      tbd
            @details    tbd
        '''
        
if __name__ == '__main__':   
    while True:    
        i2c = BNO055()
        print(i2c.readMem())
        