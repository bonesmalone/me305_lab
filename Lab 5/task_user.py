'''!@file       task_user.py
    @brief      A module task for managing state transitions and user interface.
    @details    This file contains a  in the form of a task to interpret user 
                keyboard inputs and process conditionals to perform the actions 
                of the state transitions.
    @author     Jarod Lyles
    @author     Simon Way
    @date       February 17, 2022
'''


import pyb, time, array
from shares import Share
import gc

## Assigns constant value for State 0
S0_INIT = 0
## Assigns constant value for State 1
S1_WAIT_FOR_CMD = 1
## Assigns constant value for State 2
S2_ZERO_ENC = 2
## Assigns constant value for State 3
S3_PRINT_POSITION = 3
## Assigns constant value for State 4
S4_PRINT_DELTA = 4
## Assigns constant value for State 5
S5_COLLECT_DATA = 5
## Assigns constant value for State 6
S6_SET_DUTY_CYCLE_M1 = 6
## Assigns constant value for State 7
S7_SET_DUTY_CYCLE_M2 = 7
## Assigns constant value for State 8
S8_CLEAR_FAULT = 8
## Assigns constant value for State 9
S9_GET_VELOCITY = 9
## Assigns constant value for State 10
S10_TESTING_INTERFACE = 10
## Assigns constant value for State 11
S11_RECORD_TEST = 11
## Assigns constant value for State 12
S12_PRINT_TEST = 12
## Assigns constant value for state 13
S13_CLOSEDLOOP_PT_SET = 13
## Assigns constant value for state 14
S14_ENTER_GAINS = 14
## Assigns constant value for state 15
S15_TOGGLE_CL_CONTROL = 15
## Assigns constant value for state 16
S16_STEP_RESPONCE = 16



        
def taskUser(taskName, period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, mFlag, cFlag, vFlag, tFlag, kFlag, togFlag, velData, posData, timeData, duty1, duty2, kp1, Vref):

    '''!@brief           Manages the user interfacing, state transitions, and
                         returning data to the user.
        @details         This task, taskUser(), accepts shared variables
                         for boolean flags and positional details to perform 
                         appropreiate state transitions and return values to
                         to the ueser interface.
        @param taskName  Assigns a name to the taskUser task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param position  Initializes a passed shared integer variable upon which encoder
                         position data can be assigned.
        @param delta     Initializes a passed shared integer variable for assignment of
                         position change data can be assigned.
        @param zFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for zeroing the encoder.
        @param pFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position.
        @param dFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position change.
        @param gFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for recording data over time.
        @param posData   Initializes a passed shared variable for
                         recording position data.
        @param timeData  Initializes a passed shared variable for
                         recording time data.                 
                         
    '''
    
    intro()
    
    ## Initializes an active state variable for transition management
    state = S0_INIT
    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)
    
    ## Initializes array for storing collected time data
    timeArray = array.array('f', 3000 * [0])
    gc.collect()
    
    ## Initializes array for storing collected position data
    posArray = array.array('f', timeArray)
    ## Initializes array for storing collected velocity data
    velArray = array.array('f', timeArray)
    ## Initializes the activation state of the closed loop control
    toggle = 0
    ## Initializes the activation state of the step responce function
    CL_START = 0
   
    
    ## Initializes a serial port reader for user input
    serport = pyb.USB_VCP()
    
    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        if time.ticks_diff(current_time,next_time) >= 0:
            next_time = time.ticks_add(next_time, period)
            
            if state == S0_INIT:
                state = S1_WAIT_FOR_CMD
                    
            elif state == S1_WAIT_FOR_CMD:
                if serport.any():
                    
                    ## Assigns a character variable for the read out from the serial coms
                    charIn = serport.read(1).decode()
                    
                    # The following if/else tree performs the conditional logic
                    # on the user input to determine state transtion from State 1 
                    # and properly flag for use in the task encoder.
                    if charIn in {'z', 'Z'}:
                        state = S2_ZERO_ENC
                        zFlag.write(True)
                        print('Input Z - Zeroing Encoder')
                        print('Encoder position zeroed!')
                        
                    elif charIn in {'p','P'}:
                        state = S3_PRINT_POSITION
                        pFlag.write(True)
                        print('Input P – Returning Current Encoder Position')
                        print('Position')
                        
                    elif charIn in {'d','D'}:
                        state = S4_PRINT_DELTA
                        dFlag.write(True)
                        print('Input D – Returning Encoder Position Change')
                        print('Delta')
                        
                    elif charIn in {'g','G'}:
                        state = S5_COLLECT_DATA
                        print('Input G - Collect Data')
                        collection_start = time.ticks_ms()
                        gFlag.write(True)
                        index = 0
                        
                        print('            Begin Data Collection             ')
                        print('______________________________________________')
                        print('Time [s], Position [rad], Velocity [rad/s]    ')
                       
                    elif charIn in {'h','H'}:
                        intro()
                        
                    elif charIn in {'m'}:
                        state = S6_SET_DUTY_CYCLE_M1
                        print('Input m - Enter Duty Cycle for Motor 1')
                        mFlag.write(True)

                    elif charIn in {'M'}:
                        state = S7_SET_DUTY_CYCLE_M2
                        print('Input M - Enter Duty Cycle for Motor 2')
                        mFlag.write(True)
                        
                    elif charIn in {'c', 'C'}:
                        state = S8_CLEAR_FAULT
                        print('Input C - Clear Motor Fault')
                        cFlag.write(True)
                    
                    elif charIn in {'v', 'V'}:
                        state = S9_GET_VELOCITY
                        print('Input V - Get Velocity of Motor/Encoder 1')
                        vFlag.write(True)
                    
                    elif charIn in {'t', 'T'}:
                        state = S10_TESTING_INTERFACE
                        print('Input T - Open Motor Testing Interface')
                        tFlag.write(True)
                    
                        avgVelList = []
                        dutyList = []
                        velocityList = []
                        
                    elif charIn in {'y', 'Y'}:
                        state = S13_CLOSEDLOOP_PT_SET
                        print('Enter set point for closed-loop control')
                        
                    elif charIn in {'k', 'K'}:
                        state = S14_ENTER_GAINS
                        print('Enter new gains value')
                        #kFlag.write(True)
                        
                    elif charIn in {'w', 'W'}:
                        state = S15_TOGGLE_CL_CONTROL
                        
                    elif charIn in {'r', 'R'}:
                        CL_START = 1
                        state = S14_ENTER_GAINS
                        print('Enter new gains value for Step Responce')
                        #state = S16_STEP_RESPONCE
                        
                    else:
                        print(f'You entered {charIn}. This is not a valid command.')
            
            # The following if/else tree determines appropreiate actions per 
            # state assignment
            elif state == S2_ZERO_ENC:
                if not zFlag.read():
                    state = S1_WAIT_FOR_CMD
                
            elif state == S3_PRINT_POSITION:
                if not pFlag.read():
                    print(position.read())
                    state = S1_WAIT_FOR_CMD
            
            elif state == S4_PRINT_DELTA:
                if not dFlag.read():
                    print(delta.read())
                    state = S1_WAIT_FOR_CMD
                
            elif state == S5_COLLECT_DATA:
                # Reads the serial port for potential 'S' input
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'s', 'S'}:
                        gFlag.write(False)
                        # For loop to print out time/position data if 'S' is entered
                        for (t, p, v) in zip(timeArray[:index], posArray[:index], velArray[:index]):
                            print(f'{(t - collection_start)/1000:.2f},  {p},    {v}')
                            #print(f'{(t - collection_start)/1000:.2f}, {p}')

                        print('______________________________________________')
                        print('             End Data Collection              ')
                        state = S1_WAIT_FOR_CMD
                    
                # Conditional for 3000 data entries (30 seconds given 1000us period)
                elif index >= 3000:
                    # For loop to print out time/position data after 3000 entries (30 seconds)
                    gFlag.write(False)
                    for (t, p, v) in zip(timeArray[:index], posArray[:index], velArray[:index]):
                        print(f'{(t - collection_start)/1000:.2f},  {p},    {v}')
                        #print(f'{(t - collection_start)/1000:.2f}, {p}')
                        
                    print('______________________________________________')
                    print('             End Data Collection              ')
                    state = S1_WAIT_FOR_CMD
                
                # Conditional to continue adding to the time/position array otherwise
                else:
                    timeArray[index] = timeData.read()
                    posArray[index] = (posData.read() * 2 * 3.141 / 4000)
                    velArray[index] = (velData.read() * 2 * 3.141 / 4000)
                    index += 1
            
            elif state == S6_SET_DUTY_CYCLE_M1:
                buff = ''
                numFlag = True
                while numFlag:
                    
                    if serport.any():
                        charIn = serport.read(1).decode()
                        
                        if charIn.isdigit():
                            buff += charIn
                            print(charIn, end='' )
                            
                        elif charIn in {'-'}: 
                            #nagetive input chase chaeck
                            if len(buff) == 0:
                                buff += charIn
                                print(charIn, end='')
                            else:
                                pass
                            
                        elif charIn in {'\b', '\x08', '\x7F'}: 
                            if len(buff) > 0:
                                buff = buff[:-1]
                                print('')
                                print(buff)
                            else:
                                pass
                                
                        elif charIn in {'.'}:
                            #deciaml input chaeck case
                            buff += charIn
                            print(charIn, end='')
                            
                        elif charIn in {'\n','\r'}:
                            #Enter input check case
                            if len(buff) == 0:
                                buff = '0'
                                #assumes no value enters as zero
                                #print(state)
                                
                            else:
                                numFlag = False
                            break
                
                duty = float(buff)
                
                if duty > 100:
                    duty = 100
                    print('\nDuty cycle can not exceed 100%')
                elif duty < -100:
                    duty = -100
                    print('\nDuty cycle can not exceed -100%')
                elif duty == 0:
                    duty = 0
                duty1.write(duty)
                print(f'\nMotor 1 duty cycle set to {duty1.read()}%')
                state = S1_WAIT_FOR_CMD
            
            elif state == S7_SET_DUTY_CYCLE_M2:
                buff = ''
                numFlag = True
                while numFlag:
                    
                    if serport.any():
                        charIn = serport.read(1).decode()
                        
                        if charIn.isdigit():
                            buff += charIn
                            print(charIn, end='' )
                            
                        elif charIn in {'-'}: 
                            #nagetive input chase chaeck
                            if len(buff) == 0:
                                buff += charIn
                                print(charIn, end='')
                            else:
                                pass
                            
                        elif charIn in {'\b', '\x08', '\x7F'}: 
                            if len(buff) > 0:
                                buff = buff[:-1]
                                print('')
                                print(buff)
                            else:
                                pass
                                
                        elif charIn in {'.'}:
                            #deciaml input chaeck case
                            buff += charIn
                            print(charIn, end='')
                            
                        elif charIn in {'\n','\r'}:
                            #Enter input check case
                            if len(buff) == 0:
                                buff = '0'
                                #assumes no value enters as zero
                                #print(state)
                                
                            else:
                                numFlag = False
                            break
                
                duty = float(buff)
                
                if duty > 100:
                    duty = 100
                    print('\nDuty cycle can not exceed 100%')
                elif duty < -100:
                    duty = -100
                    print('\nDuty cycle can not exceed -100%')
                elif duty == 0:
                    duty = 0
                duty2.write(duty)
                print(f'\nMotor 2 duty cycle set to {duty2.read()}%')
                state = S1_WAIT_FOR_CMD
                
                
            elif state == S9_GET_VELOCITY:
                if not vFlag.read():
                    print(f'{velocity.read() * 2 * 3.14 / 4000} [rad/s]')
                    state = S1_WAIT_FOR_CMD
            
            elif state == S8_CLEAR_FAULT:
                if not cFlag.read():
                    print('MOTOR FAULT CLEARED')
                    state = S1_WAIT_FOR_CMD
            
            elif state == S10_TESTING_INTERFACE:
                #print('Enter a duty cycle for Motor 1 or enter [S] to exit')
                numFlag = True
                buff = ''
                if numFlag:
                
                    if serport.any():
                        charIn = serport.read(1).decode()
                        
                        if charIn.isdigit():
                            buff += charIn
                            print(charIn, end='' )
                            
                        elif charIn in {'-'}: 
                            #nagetive input chase chaeck
                            if len(buff) == 0:
                                buff += charIn
                                print(charIn, end='')
                            else:
                                pass
                            
                        elif charIn in {'\b', '\x08', '\x7F'}: 
                            if len(buff) > 0:
                                buff = buff[:-1]
                                print('')
                                print(buff)
                            else:
                                pass
                                
                        elif charIn in {'.'}:
                            #deciaml input chaeck case
                            buff += charIn
                            print(charIn, end='')
                            
                        elif charIn in {'\n','\r'}:
                            #Enter input check case
                            if len(buff) == 0:
                                buff = '0'
                                #assumes no value enters as zero
                                
                            duty = float(buff)
                            dutyList.append(duty)
                            if duty > 100:
                                duty = 100
                                print('\nDuty cycle can not exceed 100%')
                            elif duty < -100:
                                duty = -100
                                print('\nDuty cycle can not exceed -100%')
                            elif duty == 0:
                                duty = 0
                            duty1.write(duty)
                            print(f'\nMotor 1 duty cycle set to {duty1.read()}%')
                            #test_start = current_time
                            
                            velocityList = []
                            numFlag = False
                            state = S11_RECORD_TEST
                        
                        elif charIn in {'s','S'}:
                            state = S12_PRINT_TEST
                            numFlag = False
                            buff = '0'
                            
    
                        else:
                            state = S10_TESTING_INTERFACE
                            
                
            elif state == S11_RECORD_TEST:
                if len(velocityList) >= 10:
                    avgVelList.append(sum(velocityList)/len(velocityList))
                    state = S10_TESTING_INTERFACE
                else:
                    vFlag.write(True)
                    velocityList.append(velocity.read()/1_000)
                #print(velocityList)

            elif state == S12_PRINT_TEST:
                if len(dutyList) > 0:
                    print('Duty Cycle [%], Speed [rad/s]')
                    for (pos, vel) in zip(dutyList, avgVelList):
                        print(f'{pos}, {vel}')
                    print('Exiting Testing Interface')
                    state = S1_WAIT_FOR_CMD
                    pass
                else:
                    state = S1_WAIT_FOR_CMD
                    
            elif state == S13_CLOSEDLOOP_PT_SET:
                #When the user presses Y or y
                if CL_START == 1:
                    print('Enter new reference velocity value for the Step Responce')
                buff = ''
                numFlag = True
                while numFlag:
                    
                    if serport.any():
                        charIn = serport.read(1).decode()
                        
                        if charIn.isdigit():
                            buff += charIn
                            print(charIn, end='' )
                            
                        elif charIn in {'-'}: 
                            #nagetive input chase chaeck
                            if len(buff) == 0:
                                buff += charIn
                                print(charIn, end='')
                            else:
                                pass
                            
                        elif charIn in {'\b', '\x08', '\x7F'}: 
                            if len(buff) > 0:
                                buff = buff[:-1]
                                print('')
                                print(buff)
                            else:
                                pass
                                
                        elif charIn in {'.'}:
                            #deciaml input chaeck case
                            buff += charIn
                            print(charIn, end='')
                            
                        elif charIn in {'\n','\r'}:
                            #Enter input check case
                            if len(buff) == 0:
                                buff = '0'
                                #assumes no value enters as zero
                                #print(state)
                                
                            else:
                                numFlag = False
                            break
                
                ref = float(buff) * 4000 / (2 * 3.14)
                Vref.write(ref)
                print(f'\nReference Velocity set to {float(buff)} rad/s')
                
                ## Routes the code to the correct next step depending on the step responce
                if CL_START == 1:
                    state = S16_STEP_RESPONCE
                else:
                    state = S1_WAIT_FOR_CMD
            
            elif state == S14_ENTER_GAINS:
                #when the user enters k or K
                buff = ''
                numFlag = True
                while numFlag:
                    
                    if serport.any():
                        charIn = serport.read(1).decode()
                        
                        if charIn.isdigit():
                            buff += charIn
                            print(charIn, end='' )
                            
                        elif charIn in {'-'}: 
                            #nagetive input chase chaeck
                            if len(buff) == 0:
                                buff += charIn
                                print(charIn, end='')
                            else:
                                pass
                            
                        elif charIn in {'\b', '\x08', '\x7F'}: 
                            if len(buff) > 0:
                                buff = buff[:-1]
                                print('')
                                print(buff)
                            else:
                                pass
                                
                        elif charIn in {'.'}:
                            #deciaml input chaeck case
                            buff += charIn
                            print(charIn, end='')
                            
                        elif charIn in {'\n','\r'}:
                            #Enter input check case
                            if len(buff) == 0:
                                buff = '0'
                                #assumes no value enters as zero
                                #print(state)
                                
                            else:
                                numFlag = False
                            break
                
                kp = float(buff)
                
                if kp > 100:
                    kp = 100
                    print('\nGain Value can not exceed 100%')
                elif kp < -100:
                    kp = -100
                    print('\nGain Value can not exceed -100%')
                elif kp == 0:
                    kp = 0
                kp1.write(kp)
                kFlag.write(True)
                
                if CL_START == 1:
                    state = S13_CLOSEDLOOP_PT_SET
                    #print('\nEnter new reference velocity for the Step Responce')
                else:
                    state = S1_WAIT_FOR_CMD
            
            elif state == S15_TOGGLE_CL_CONTROL:
                ##when the user enters w or W
                if toggle == 1:
                    print('Closed Loop control disabled')
                    ## Here write a variable to false to disable it for the step responce
                    toggle = 0
                    togFlag.write(False)
                elif toggle == 0:
                    print('Closed Loop control enabled')
                    ## Here write a variable to true to enable it for the step responce
                    toggle = 1
                    togFlag.write(True)
                state = S1_WAIT_FOR_CMD
                pass
            
            elif state == S16_STEP_RESPONCE:
                ##when the user enters r or R
                print('Youve correctly routed to the step responce')
                CL_START = 0
                state = S1_WAIT_FOR_CMD
                pass

            else:
                raise ValueError(f'Invalid state value in {taskName}: State {state} does not exist')
                
            yield state
            
        else:
            yield None
    

# Function for printing the welcome / command window
def intro():
    print(' _____________________________________________________ ')
    print('|                                                     |')
    print('|            MOTOR ENCODER TESTING PLATFORM           |')
    print('|_____________________________________________________|')
    print('| Available user commands:                            |')
    print('|  [Z] or [z]   |   Zero the Encoder 1                |')
    print('|  [P] or [p]   |   Print Encoder 1 position          |')
    print('|  [D] or [d]   |   Print the change in position      |')
    print('|  [V] or [v]   |   Print Velocity for Encoder 1      |')
    print('|         [m]   |   Enter Motor 1 duty cycle          |')
    print('|  [M]          |   Enter Motor 2 duty cycle          |')
    print('|  [C] or [c]   |   Clear motor fault condition       |')
    print('|  [G] or [g]   |   Collect 30 seconds of data        |')
    print('|  [T] or [t]   |   Open motor testing interface      |')
    print('|  [S] or [s]   |   End data collection prematurely   |')
    print('|  [Y] or [y]   |   Set point for Closed Loop control |')
    print('|  [K] or [k]   |   Enter New Gains                   |')
    print('|  [W] or [w]   |   Toggle Closed Loop Control        |')
    print('|  [R] or [r]   |   Perform motor 1 Step Responce     |')
    print('|  [H] or [h]   |   Provide help menu                 |')
    print('|  [CTRL]+[C]   |   Interrupt program                 |')
    print('|_____________________________________________________|\n')