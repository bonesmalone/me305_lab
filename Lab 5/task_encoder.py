'''!@file       task_encoder.py
    @brief      A module for tasking the encoder and closedloop divers to perform functions.
    @details    This file contains a module in the form of a task that intializes
                the encoder driver and appropriately perform functions according to
                and a task for modifing the conditons of the closed loop control.
    @author     Jarod Lyles
    @author     Simon Way
    @date       February 2, 2022
'''

import pyb, time, encoder, ClosedLoop
from shares import Share

## Initializes pin B6 on the Nucleo for interface with the first encoder
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
## Initializes pin B6 on the Nucleo for interface with the first encoder
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)

## Initializes pin C6 on the Nucleo for interface with the second encoder
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
## Initializes pin C7 on the Nucleo for interface with the second encoder
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
             

def taskEncoder(taskName, period, position, delta, velocity, zFlag, pFlag, dFlag, gFlag, vFlag, velData, posData, timeData):
    
    '''!@brief           Calls an instance of the encoder class and tasks the 
                         encoder to perform functions accordingly.
        @details         This function, taskEncoder(), accepts shared variables
                         for boolean flags and positional details, and uses 
                         these flags and positional variables to assign values
                         from appropreiate user input.
        @param taskName  Assigns a name to the taskEncoder task.
        @param period    Provides the period of update in microseconds for the 
                         task encoder function.
        @param position  Initializes a passed shared integer variable upon which encoder
                         position data can be assigned.
        @param delta     Initializes a passed shared integer variable for assignment of
                         position change data can be assigned.
        @param zFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for zeroing the encoder.
        @param pFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position.
        @param dFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for getting encoder position change.
        @param gFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for recording data over time.
        @param vFlag     Initializes a passed shared boolean flag for
                         evaluation and assignment of user input details,
                         specifically for passing velocity reading.
        @param posData   Initializes a passed shared variable for
                         recording position data.
        @param timeData  Initializes a passed shared variable for
                         recording time data.                 
                         
    '''
    
    ## Initializes one instance of encoder class object
    encoder1 = encoder.Encoder(pinB6, pinB7, 4)
    ## Initializes another instance of encoder class object
    #encoder2 = encoder.Encoder(pinC6, pinC7, 8)
    
    ## Initializes start time for the task encoder function
    start_time = time.ticks_us()
    ## Initializes time interval of update for the task encoder
    next_time = time.ticks_add(start_time, period)
    ## Converts period from micro seconds to seconds
    ##delta_t = period / 1000000
    prev_time = time.ticks_us()
    
    while True:
        ## Provides time variable for the run time of the task loop
        current_time = time.ticks_us()
        
        # Updates the encoder
        encoder1.update()
        
        # Performs conditional assignment of encoder functions
        if current_time >= next_time:
            next_time = time.ticks_add(start_time, period)
            delta_time = time.ticks_diff(current_time, prev_time) / 1_000_000
            velocity.write(encoder1.get_delta()/delta_time)
            # Tasks the encoder for zeroing of position
            if zFlag.read():
                encoder1.zero(0)
                zFlag.write(False)
            
            # Tasks the encoder to get position
            elif pFlag.read():
                position.write(encoder1.get_position())
                pFlag.write(False)
            
            # Tasks the encoder to get the instantaneous postional delta
            elif dFlag.read():
                delta.write(encoder1.get_delta())
                dFlag.write(False)
            
            # Tasks the encoder to collect data – update and store values in arrays 
            elif gFlag.read():
                encoder1.update()
                timeData.write(time.ticks_ms())
                ##posData.write(encoder1.get_delta())
                posData.write(encoder1.get_position())
                delta_time = time.ticks_diff(current_time, prev_time) / 1_000_000
                velData.write(encoder1.get_delta()/delta_time)
                #calling delta on a time interval that deosnt match the period?
                #need to edit the time interval for checking delta?
            
            # Tasks the encoder to provide velocity values – update and store values in arrays 
            elif vFlag.read():
                # Velocity = delta / time period of update
                delta_time = time.ticks_diff(current_time, prev_time) / 1_000_000
                velocity.write(encoder1.get_delta()/delta_time)
                vFlag.write(False)
            
            # Saves the current state of the function and returns values
            yield None
            prev_time = current_time    
        else:
            yield None
                    
            
def taskClosedLoop(taskname, period, velocity, kFlag, kp1, Vref, LAct):
    
    '''!@brief              Manages closed loop control and calibration
        @details            This task, task_ClosedLoop(), uses shared variables and boolean
                            flags for actuation values and calibration details to 
                            perform appropriate state transitions and return adjusted
                            values of duty cycle to the circuit when active. 
        @param taskname     Assigns name to the taskClosedLoop task.
        @param period       Provides the period of update in microseconds
        @param kFlag        Initializes a passed shared boolean flag for evaluation
                            and assignment of user input details, specifically for
                            seting the gain value.
                            
    '''
    
    ClosedLoop1 = ClosedLoop.Closed_Loop()
    
    start_time = time.ticks_us()
    
    next_time = time.ticks_add(start_time, period)
    
    prev_time = time.ticks_us()
    
    while True:
        current_time = time.ticks_us()
        
        #ClosedLoop1.run(Vref.read(), velocity.read())
        #delt_time = time.ticks_diff(current_time, prev_time) / 1_000_000
        LAct.write(ClosedLoop1.run(Vref.read(), velocity.read()))
        
        if current_time >= next_time:
            next_time = time.ticks_add(start_time, period)
        
            if kFlag.read():
                ##print('Hey you get how flags work. Good job dude!')
                ClosedLoop1.set_kp(kp1.read())
                print(f'\nKp value set to {kp1.read()} %s/rad')
                kFlag.write(False)
            yield None
            prev_time = current_time
        else:
            yield None