'''!@file       ClosedLoop.py
    @brief      tbd
    @details    tbd
    @author     Simon Way
    @date       Febuary 19, 2022
'''

import pyb
import shares

class Closed_Loop:
    '''!@brief          Apply closed loop control to board motors
        @details        This class controls/adjusts the duty cycles applied to board motors and performs error and gain calculations.
    '''
    
    def __init__(self):
        '''!@brief      Contructs the Closed Loop object
            @details    Initiates the Close Loop object by defining object parameters
        '''
        #Here is where the important stuff gets defined
        #Initializes gain value for calculations
        self.k_p = 0
        #Initializes value for actuation
        self.actuation = 0
        
    def run(self, ref, meas):
        '''!@brief      returns actuation values
            @details    This function calculates and returns the actuation values from the input.
        '''
        #computes and returns actuation values (duty cycle applied to motor)        
        self.err = ref - meas
        if self.err < 0 and ref > 0:
            ##If the error is negative when the desired velocity is positive
            self.err = self.err * -1
        elif self.err > 0 and ref < 0:
            ##If the error is positive when the desired velocity is negative
            self.err = self.err * -1
        else:
            pass
        
        self.actuation = self.err * self.k_p
        
        return self.actuation
        
    def set_kp(self, kp_in):
        '''!@brief      Sets gain value
            @details    This function modifies the gain value from the input
        '''
        #Allows for modification of the gain value
        
        self.k_p = kp_in * 2 * 3.14 / 4000
        
        