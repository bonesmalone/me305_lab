import time
import math
import pyb

def onButtonPressCallback (IRQ_src):
    global buttonPressed
    buttonPressed = True;
    return

def SquareWave(t):
    toggle = 0
    var = int(t) %(2)   #determines if even or odd
    if var == 0:        #even vlaues
            toggle = 0
    elif var == 1:      #Odd values
            toggle = 100
    return toggle

def SineWave(t):
    return math.sin(2*3.1415*t)

def SawWave(t):
    pass

if __name__ == '__main__':
    buttonPressed = False
    state = 0

    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 0.5)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, 
                                       pull=pyb.Pin.PULL_NONE,
                                       callback=onButtonPressCallback)
    
    while True:
        try:
            
            if state == 0:
                #run state zero
                print ('To Begin, Please Press Blue User Interface Button')
                if buttonPressed:
                    buttonPressed = False
                    state = 1
            elif state == 1:
                #run square wave
                tog = SquareWave(time.ticks_ms())
                t2ch1.pulse_width_percent(tog)
                if buttonPressed:
                    buttonPressed = False
                    state = 2
            elif state == 2:
                #run sine wave
                sine = SineWave(time.tick_ms())
                t2ch1.pulse_width_percent(sine)
                pass
            elif state == 3:
                #run saw wave
                pass
            
            time.sleep(1)
        except KeyboardInterrupt:
            break
    print('ENDING OPERATIONS')